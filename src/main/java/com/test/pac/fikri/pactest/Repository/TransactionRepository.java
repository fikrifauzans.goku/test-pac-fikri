package com.test.pac.fikri.pactest.Repository;

import com.test.pac.fikri.pactest.Entity.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {
}

