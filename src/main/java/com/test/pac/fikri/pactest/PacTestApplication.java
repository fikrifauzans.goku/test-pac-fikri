package com.test.pac.fikri.pactest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PacTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(PacTestApplication.class, args);
	}

}
