package com.test.pac.fikri.pactest.Entity;

public enum TransactionType {
    TARIK_TUNAI,
    SETOR_TUNAI,
    TRANSFER
}
