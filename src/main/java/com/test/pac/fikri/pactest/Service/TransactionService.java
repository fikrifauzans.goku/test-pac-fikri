package com.test.pac.fikri.pactest.Service;

import com.test.pac.fikri.pactest.Entity.Account;
import com.test.pac.fikri.pactest.Entity.Transaction;
import com.test.pac.fikri.pactest.Model.TransactionModel;
import com.test.pac.fikri.pactest.Repository.AccountRepository;
import com.test.pac.fikri.pactest.Repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;

@Service
public class TransactionService {

    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private TransactionRepository transactionRepository;

    public void tarikTunai(TransactionModel req, Long id) {
        Account user = accountRepository.findById(id)
                .orElseThrow( () -> new IllegalArgumentException("Invalid transaction id" + id));

        Transaction trx = new Transaction();
        BigDecimal transaksi = user.getSaldo().subtract(req.getNominal());
        user.setSaldo(transaksi);
        trx.setJenisTransaksi(req.getTrxType());
        trx.setTanggal(new Date());
        trx.setNominal(req.getNominal());
        trx.setTipe("DEBIT");

        transactionRepository.save(trx);
        accountRepository.save(user);
    }

    public void setorTunai(TransactionModel req, Long id) {
        Account user = accountRepository.findById(id)
                .orElseThrow( () -> new IllegalArgumentException("Invalid transaction id" + id));

        Transaction trx = new Transaction();
        BigDecimal transaksi = user.getSaldo().add(req.getNominal());
        user.setSaldo(transaksi);
        trx.setJenisTransaksi(req.getTrxType());
        trx.setTanggal(new Date());
        trx.setNominal(req.getNominal());
        trx.setTipe("KREDIT");

        transactionRepository.save(trx);
        accountRepository.save(user);
    }

}
