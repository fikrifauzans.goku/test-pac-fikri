package com.test.pac.fikri.pactest.Repository;

import com.test.pac.fikri.pactest.Entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {
}
