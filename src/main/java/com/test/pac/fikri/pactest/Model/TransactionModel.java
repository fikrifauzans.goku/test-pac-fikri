package com.test.pac.fikri.pactest.Model;


import com.test.pac.fikri.pactest.Entity.TransactionType;

import java.math.BigDecimal;

public class TransactionModel {
    private BigDecimal nominal;
    private TransactionType trxType;

    public BigDecimal getNominal() {
        return nominal;
    }

    public void setNominal(BigDecimal nominal) {
        this.nominal = nominal;
    }

    public TransactionType getTrxType() {
        return trxType;
    }

    public void setTrxType(TransactionType trxType) {
        this.trxType = trxType;
    }
}
