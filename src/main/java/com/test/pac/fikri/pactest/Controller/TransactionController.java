package com.test.pac.fikri.pactest.Controller;

import com.test.pac.fikri.pactest.Entity.Transaction;
import com.test.pac.fikri.pactest.Entity.TransactionType;
import com.test.pac.fikri.pactest.Model.TransactionModel;
import com.test.pac.fikri.pactest.Repository.TransactionRepository;
import com.test.pac.fikri.pactest.Service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
@RequestMapping("/transaction")
public class TransactionController {

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private TransactionRepository transactionRepository;

    @PostMapping("/process")
    public ResponseEntity<String> doTransaction(@ModelAttribute TransactionModel req, @RequestParam Long userId) {
        if (req.getTrxType().equals(TransactionType.SETOR_TUNAI)) {
            transactionService.setorTunai(req, userId);
            return ResponseEntity.ok("Setoran tunai berhasil.");
        } else if (req.getTrxType().equals(TransactionType.TARIK_TUNAI)) {
            transactionService.tarikTunai(req, userId);
            return ResponseEntity.ok("Penarikan tunai berhasil.");
        } else {
            return ResponseEntity.badRequest().body("Jenis transaksi tidak valid.");
        }
    }

    @GetMapping()
    public String showTransactionPage(Model model) {
        List<Transaction> transactions = transactionRepository.findAll();
        model.addAttribute("transactions", transactions);
        return "transaction";
    }
}



