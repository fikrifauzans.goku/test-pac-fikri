package com.test.pac.fikri.demo.Algoritma;

public class JavaIO {
    public static void main(String[] args) {
        String inputData = "saya sedang belajar berenang";

        String outputData = reverseString(inputData);

        System.out.println("Input data : " + inputData);
        System.out.println("Output data: " + outputData);
    }

    private static String reverseString(String input) {
        char[] charArray = input.toCharArray();
        int left = 0;
        int right = charArray.length - 1;

        while (left < right) {
            char temp = charArray[left];
            charArray[left] = charArray[right];
            charArray[right] = temp;

            left++;
            right--;
        }

        return new String(charArray);
    }
}
