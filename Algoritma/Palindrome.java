package com.test.pac.fikri.demo.Algoritma;

public class Palindrome {
    public static void main(String[] args) {
        String[] sample = {"KATAK", "MAKAN", "MALAM"};

        for (String output : sample) {
            if (isPalindrom(output)) {
                System.out.println(output + " merupakan palindrom");
            } else {
                System.out.println(output + " bukan palindrom");
            }
        }
    }

    static boolean isPalindrom(String kata) {
        int stringLength = kata.length();
        for (int i = 0; i < stringLength / 2; i++) {
            if (kata.charAt(i) != kata.charAt(stringLength - 1 - i)) {
                return false;
            }
        }
        return true;
    }

}
